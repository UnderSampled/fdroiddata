Categories:Sports & Health
License:GPLv3+
Web Site:https://www.secuso.informatik.tu-darmstadt.de/en/research/results/privacy-friendly-apps/
Source Code:https://github.com/SecUSo/privacy-friendly-activity-tracker
Issue Tracker:https://github.com/SecUSo/privacy-friendly-activity-tracker/issues

Auto Name:Pedometer
Summary:Count steps
Description:
Count your steps in background to provide you an overview about your walked
steps, distance and about the calories you have burned while walking. The app
allows you to define custom walking modes and notifications if the achievement
of your daily step goal is in danger.  You can add and choose different walking
modes, such as walking or running, to improve the calculation of distance and
calories. In a special training mode you can track your training sessions. To
each training session additional information, such as feeling or a description,
can be added.

The app requires minimal permissions (Run at startup and prevent phone from
sleeping). It belongs to the group of Privacy Friendly Apps developed at the
[https://www.secuso.informatik.tu-darmstadt.de/en/secuso-home/ SeCuSo research
group] of Technische Universitaet Darmstadt.
.

Repo Type:git
Repo:https://github.com/SecUSo/privacy-friendly-activity-tracker

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
